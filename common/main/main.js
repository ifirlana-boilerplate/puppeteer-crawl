const puppeteer = require('puppeteer');
const { Administrative } = require("../repositories/administrative");
const db = require('../database/models');

const main = async ({ config, Handlers, url, name } ) => {

  const browser = await puppeteer.launch(config);
  const page = await browser.newPage();

  try {
    await page.goto(url,{
      waitUntil: 'load',
      timeout: 0
    });
    if (config.screenshot) {
      await page.screenshot({path: `${process.cwd()}/screenshot/${name}.png`});
    }

    const administrative = new Administrative({model: db.articles});
    const handlers = new Handlers(page, config, administrative);

    await handlers.WrapHandlers();

    await browser.close();
  } catch (e) {
    console.log(e);
    if (config.screenshot) {
      await page.screenshot({path: `${process.cwd()}/screenshot/${name}.png`});
    }
    await browser.close();
  }
};

module.exports = {
  main,
}
