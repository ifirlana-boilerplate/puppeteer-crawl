class Administrative {
    constructor({model}) {
        this.model = model
    }
    createNews = async (params) => {
        if (typeof params !== 'object')
            return null;

        await this.model.findOrCreate({ where: { title: params['title'], link: params['link'] }, defaults: {
                created_time : params.created_time ? params.created_time : new Date(),
                title: params['title'],
                subtitle: params['subtitle'] ? params['subtitle'] : null,
                content: params['content'] ? params['content'] : null,
                categories: params['categories'] ? params['categories'] : null,
                author: params['author'] ? params['author'] : null,
                link: params['link'],
            }
        });
    }
    createMultipleNews = async (params) => {
        if (!Array.isArray(params))
            return null;

        for await (const param of params) {
            await this.createNews(param)
        }
    }
}
exports.Administrative = Administrative
