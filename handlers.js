const { CommonHandlers } = require('./common/main/handlers')

class Handlers extends CommonHandlers {
    constructor(page, config, administrativeRepository) {
        super()
        this.page = page;
        this.config = config;
        this.administrative = administrativeRepository;
    };

    WrapHandlers = async () => {
        // Do something here...
    }
}
module.exports = {
    Handlers,
};
